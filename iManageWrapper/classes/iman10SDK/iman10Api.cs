﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iManage.Work.Tools.Host;

namespace iManageWrapper.classes.iman10SDK
{
    internal class iman10Api : WRequesterBase2
    {
        private int returnCode;
        public bool isValid;
        private string responseAsJson;

        public iman10Api(IWHostSession2 session) : base(session)
        {
            returnCode = 0;
            responseAsJson = string.Empty;
            isValid = false;
            if (session != null)
            {
                isValid = true;
            }
        }

        public (int,string) restV2QueryGet(string apiQuery)
        {
            //string resource = @"https://imanage.bowlesrice.com/api/v2/customers/1/libraries/" + PreferredLibrary + "/users/me";

            returnCode = Requester.GetRequest(string.Format(apiQuery, PreferredLibrary), ref responseAsJson);

            return (returnCode,responseAsJson);
        }

        public (int, string) restV2QueryPatch(string apiQuery, string inputJson)
        {
            returnCode = Requester.PatchRequest(string.Format(apiQuery, PreferredLibrary), inputJson, ref responseAsJson);

            return (returnCode, responseAsJson);
        }
    }
}
