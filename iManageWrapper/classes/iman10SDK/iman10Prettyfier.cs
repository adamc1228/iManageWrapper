﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.classes.iman10SDK
{
    /*
     * 
     * 
     *  Sample code from Imanage Code examples.  
     * 
     * 
     */

    internal class iman10Prettyfier
    {
        const string INDENT_STRING = "  ";

        public static string FormatJson(string input)
        {
            int quoteCount = 0;
            int indentation = 0;

            var result =
                from ch in input
                let quotes = ch == '"' ? ++quoteCount : quoteCount
                let allowChange = (quotes % 2 == 0) ? true : false
                let openChar = (ch == '{' || ch == '[') ? true : false
                let closeChar = (ch == '}' || ch == ']') ? true : false
                let breakChar = (ch == ',') ? true : false
                let insertLinebreak = (allowChange && (openChar || closeChar || breakChar)) ? true : false
                let insertWhitespace = (allowChange && ch == ':') ? true : false
                let indent = (insertLinebreak && breakChar) ? String.Concat(Enumerable.Repeat(INDENT_STRING, indentation)) : ""
                let openIndent = (insertLinebreak && openChar) ? String.Concat(Enumerable.Repeat(INDENT_STRING, ++indentation)) : ""
                let closeIndent = (insertLinebreak && closeChar) ? String.Concat(Enumerable.Repeat(INDENT_STRING, --indentation)) : ""
                where !allowChange || !Char.IsWhiteSpace(ch)
                select insertLinebreak ?
                            (closeChar ? Environment.NewLine + closeIndent + ch
                                : (openChar ? ch + Environment.NewLine + openIndent
                                    : ch + Environment.NewLine + indent))
                            : (insertWhitespace ? " " + ch + " " : ch.ToString());

            return String.Concat(result);
        }

        public static string FormatReturnCode(int code)
        {
            if (code < 200)
                return string.Format("Return code: {0}", (iManage.IWInterfaces.IWEnumLoginStatusCode)code);
            else
                return string.Format("Return code: {0}", (System.Net.HttpStatusCode)code);
        }
    }
}
