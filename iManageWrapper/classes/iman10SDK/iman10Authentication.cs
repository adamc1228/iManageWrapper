﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iManage.Work.Tools.Host;
using iManage.Work.Tools;
using iManageWrapper.classes;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Authenticators.OAuth2;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace iManageWrapper.classes.iman10SDK
{
    internal class iman10Authentication
    {
        public List<IWHostSession2> imanWorkSessions { get; set; }

        public iman10Authentication()
        {
            imanWorkSessions = new List<IWHostSession2>();
        }


        //Connect to Imanage using Rest to get the auth token. 
        public bool loginRest(string serverURL, bool oAuth2 = false, string clientID = null)
        {

            string serverUrl = serverURL.Trim().TrimEnd('/');
            if (!string.IsNullOrEmpty(serverUrl))
            {
                if (!serverUrl.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase))
                    serverUrl = "https://" + serverUrl;

                //Ensure previous session is disconnected
                logoutRest(serverUrl);
            }
            
            IWHostSession2 session;

            if (oAuth2 && !string.IsNullOrEmpty(clientID))
            {
                //OAuth Login 
                System.Diagnostics.Debug.WriteLine("Debug - iManage - Attempting OAuth2 Login", true);
                session = string.IsNullOrEmpty(serverUrl) ?
                            WHostFactory.CreateEmptyOAuth2Session(clientID) :
                            WHostFactory.CreateOAuth2Session(serverUrl, clientID);

                System.Diagnostics.Debug.WriteLine(session);
            }
            else
            {
                //Standard Login
                System.Diagnostics.Debug.WriteLine("Debug - iManage - Attempting Traditional Login", true);
                session = string.IsNullOrEmpty(serverUrl) ? WHostFactory.CreateEmptySession2() : WHostFactory.CreateSession2(serverUrl);
            }

            // quickly check if server is reachable
            if (!session.IsServerReachable)
            {
                System.Diagnostics.Debug.WriteLine("Error - iManage - Server Unavailable", false,true);
                return false;
            }

            if (session.Connect())
            {
                imanWorkSessions.Add(session);

                System.Diagnostics.Debug.WriteLine(string.Format("Token length: {0}\nUser: {1} ({2})\nREST: {3}\nError: {4}\nError-Code:{5}\nAuthToken:{6}\nRestHostUrl:{7}\nUrl:{8}\nUrl Prefix:{9}\nCustomer Rest:{10}",
                    session.AuthToken.Length,
                    session.UserId, session.UserEmail,
                    session.CustomerRestEndPoint, 
                    session.LastError,
                    session.LastErrorCode,
                    session.AuthToken,
                    session.RestHostUrl,
                    session.Url, 
                    session.WebUrlPrefix,
                    session.CustomerRestEndPoint),true,false);
                

                return true;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error - iManage - Login Failure, unable to aquire token!", false, true);
                System.Diagnostics.Debug.WriteLine(string.Format("connection failed\nError code: {0}\nError: {1}", session.LastErrorCode, session.LastError), true, true);
            }

            return false;
        }

        public string getRestEndpoint(IWHostSession2 session)
        {
            return session.CustomerRestEndPoint;
        }

        //Logout of Imanage.
        public void logoutRest(string serverUrl)
        {
            if (string.IsNullOrEmpty(serverUrl))
                return;

            //Search each session for logout url
            IWHostSession2 matched = null;
            foreach (IWHostSession2 session in imanWorkSessions)
            {
                if (string.Compare(session.Url, serverUrl, true) == 0)
                    matched = session;
            }
            if (matched == null)
                return;

            StringBuilder result = new StringBuilder();

            //Trigger Disconnect
            if (matched.Disconnect())
                result.AppendFormat("disconnected: {0}", matched.Url);
            else
                result.AppendFormat("{0}\n{1}", iman10Prettyfier.FormatReturnCode(matched.LastErrorCode), matched.LastError);

            imanWorkSessions.Remove(matched);
        }
    }
}
