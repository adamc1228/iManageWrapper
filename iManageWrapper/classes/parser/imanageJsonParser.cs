﻿using iManageWrapper.dataStructures;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.classes.parser
{
    public static class imanageJsonParser
    {

        public static bool debugOutput { get; set; }

        //Parse the json into a list of user accounts.
        public static accounts parseUsers(string rawJson)
        {
            try
            {
                dynamic parsedJson = JsonConvert.DeserializeObject(rawJson);

                if (debugOutput)
                    System.Diagnostics.Debug.WriteLine(((Newtonsoft.Json.Linq.JObject)parsedJson).ToString());

                return JsonConvert.DeserializeObject<accounts>(rawJson);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception occurred parsing user accounts + \n" + ex.StackTrace);
                return new accounts();
            }
        }

        public static docProfiles parseDocProfiles(string rawJson)
        {
            try
            {
                dynamic parsedJson = JsonConvert.DeserializeObject(rawJson);

                if(debugOutput)
                    System.Diagnostics.Debug.WriteLine(((Newtonsoft.Json.Linq.JObject)parsedJson.data).ToString());

                return JsonConvert.DeserializeObject<docProfiles>(((Newtonsoft.Json.Linq.JObject)parsedJson.data).ToString());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception occured parsing document profiles + \n" + ex.StackTrace);
                return new docProfiles();
            }
        }

        public static docHistory parseDocHistory(string rawJson)
        {
            try
            {
                dynamic parsedJson = JsonConvert.DeserializeObject(rawJson);
                //System.Diagnostics.Debug.WriteLine(((Newtonsoft.Json.Linq.JArray)parsedJson.data).ToString());
                return JsonConvert.DeserializeObject<docHistory>(rawJson);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception occured parsing document profiles + \n" + ex.StackTrace);
                return null;
            }
        }

        public static docHistoryRecord getCheckoutRecord(docProfile currentDoc)
        {
            if (currentDoc != null && currentDoc.DocumentHistory != null && currentDoc.DocumentHistory.Records != null)
            {
                foreach (docHistoryRecord currentRecord in currentDoc.DocumentHistory.Records)
                {
                    if (currentRecord.Activity.ToLower().Trim() == "checkout")
                    {
                        return currentRecord;
                    }
                }
            }
            return null;
        }
    }
}
