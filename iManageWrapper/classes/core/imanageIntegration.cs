﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Web;
using iManageWrapper.classes.iman10SDK;

namespace iManageWrapper.classes.core
{
    public class imanageIntegration
    {
        private iman10Authentication imanageCon;
        private iman10Api imanageRestTool;
        private bool connectionStatus;

        private string iManageServer;
        private string clientToken;
        private bool oAuth2;

        public bool validConnection { get { return connectionStatus; } }

        public imanageIntegration(string _iManageServer, string _clientToken, bool _oAuth2)
        {
            connectionStatus = false;
            clientToken = _clientToken;
            iManageServer = _iManageServer;
            oAuth2 = _oAuth2;

            //Create Authentication object
            imanageCon = new iman10Authentication();

            //Determine type of authentication
            if (oAuth2)
            {
                //Establish OAuth2 Session
                if (imanageCon.loginRest(iManageServer, true, clientToken))
                    if(imanageCon.imanWorkSessions[0]!=null)
                    {
                        //This assumes that we only allow a single imanage conneciton. Which we do. For now...
                        imanageRestTool = new iman10Api(imanageCon.imanWorkSessions[0]);
                        connectionStatus = true;
                    }
            }
            else
            {
                //Establish Traditional Login session
                if (imanageCon.loginRest(iManageServer))
                    if (imanageCon.imanWorkSessions[0] != null)
                    {
                        //This assumes that we only allow a single imanage conneciton. Which we do. For now...
                        imanageRestTool = new iman10Api(imanageCon.imanWorkSessions[0]);
                        connectionStatus = true;
                    }
            }
        }

        public bool reconnect()
        {
            imanageCon.logoutRest(iManageServer);

            //Determine type of authentication
            if (oAuth2)
            {
                //Establish OAuth2 Session
                if (imanageCon.loginRest(iManageServer, true, clientToken))
                    if (imanageCon.imanWorkSessions[0] != null)
                    {
                        //This assumes that we only allow a single imanage conneciton. Which we do. For now...
                        imanageRestTool = new iman10Api(imanageCon.imanWorkSessions[0]);
                        connectionStatus = true;
                    }
            }
            else
            {
                //Establish Traditional Login session
                if (imanageCon.loginRest(iManageServer))
                    if (imanageCon.imanWorkSessions[0] != null)
                    {
                        //This assumes that we only allow a single imanage conneciton. Which we do. For now...
                        imanageRestTool = new iman10Api(imanageCon.imanWorkSessions[0]);
                        connectionStatus = true;
                    }
            }

            return connectionStatus;
        }



        //@"/libraries/{0}/users?limit=9000"
        public (int jsonResult, string jsonData) restV2QueryGet(string query)
        {
            try
            {
                //Submit Query to Imanage
                return imanageRestTool.restV2QueryGet(imanageCon.getRestEndpoint(imanageCon.imanWorkSessions[0]) + query);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error - ImanageIntegration - Unable to query iManage", false, true);
                System.Diagnostics.Debug.WriteLine("Stacktrace:\n" + ex.StackTrace, true, true, false);
                return (-1,"");
            }
        }

        public (int jsonResult, string jsonData) restV2QueryPatch(string query, string jsonData)
        {
            try
            {
                return imanageRestTool.restV2QueryPatch(imanageCon.getRestEndpoint(imanageCon.imanWorkSessions[0]) + query, jsonData);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error - ImanageIntegration - Unable to query iManage", false, true);
                System.Diagnostics.Debug.WriteLine("Stacktrace:\n" + ex.StackTrace, true, true, false);
                return (-1, "");
            }
        }
        private static string sanitizedString(string valueTextForCompiler)
        {
            return System.Web.HttpUtility.JavaScriptStringEncode(valueTextForCompiler);
        }
    }
}
