﻿using Microsoft.Win32;
using System;
using System.Security;

namespace iManageWrapper.classes.core
{
    public class imanageSettings
    {
        //Imanage Accounts
        public bool accountInfo_Imanage_OAuth2 { get; set; }
        public string accountInfo_Imanage_Server { get; set; }
        public string accountInfo_Imanage_Client { get; set; }
        public string accountInfo_Imanage_Secret { get; set; }

        public string accountInfo_Imanage_Account { get; set; }
        public SecureString accountInfo_Imanage_Pass { get; set; }

        public imanageSettings()
        {

            //Imanage Processing
            accountInfo_Imanage_OAuth2 = true;
            accountInfo_Imanage_Server = "";
            accountInfo_Imanage_Client = "";
            accountInfo_Imanage_Secret= "";
        }

        public bool saveSettings()
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\BowlesRice\imanageWrapper");

                //Imanage Processing
                key.SetValue("imanageSecret", accountInfo_Imanage_Secret, RegistryValueKind.String);
                key.SetValue("imanageServer", accountInfo_Imanage_Server, RegistryValueKind.String);
                key.SetValue("imanageClient", accountInfo_Imanage_Client, RegistryValueKind.String);

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                return false;
            }
        }

        public bool loadSettings()
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\BowlesRice\imanageWrapper");

                if (key == null)
                    return false;

                //Imanage Processing
                if (key.GetValue("imanageSecret") != null)
                    accountInfo_Imanage_Secret = Convert.ToString(key.GetValue("imanageSecret"));
                if (key.GetValue("imanageServer") != null)
                    accountInfo_Imanage_Server = Convert.ToString(key.GetValue("imanageServer"));
                if (key.GetValue("imanageClient") != null)
                    accountInfo_Imanage_Client = Convert.ToString(key.GetValue("imanageClient"));

                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                return false;
            }
        }

        public void updateSettings()
        {
            imanageLoginForm loginForm = new imanageLoginForm(this);
            loginForm.Show();
            loginForm.updateSettings();
        }

        public void updateSettingsPass()
        {
            imanageLoginFormPass loginForm = new imanageLoginFormPass(this);
            loginForm.ShowDialog();
            loginForm.updateSettings();
            accountInfo_Imanage_Account = loginForm.tbAccount.Text;
            accountInfo_Imanage_Pass = loginForm.passCode;
        }
    }
}
