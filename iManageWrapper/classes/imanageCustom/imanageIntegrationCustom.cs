﻿using Newtonsoft.Json;
using System;
using iManageWrapper.classes.imanageCustom.rest;
using iManageWrapper.dataStructures;
using System.Security;
using RestSharp;
using System.Net;

namespace iManageWrapper.classes.imanageCustom
{
    public class imanageIntegrationCustom
    {
        private customRestClient imanageCon;

        private bool connectionStatus;
        private imanSessionInfo imanSession;
        private authToken accessToken;
        private string baseUrl;
        private bool oAuth2;


        public bool validConnection { get { return connectionStatus; } }

        public imanageIntegrationCustom(string _iManageServer, string _user, SecureString _password, string _clientToken, string _clientSecret, string _persona="admin", bool _oAuth2 = true)
        {
            imanageCon = new customRestClient();
            baseUrl = _iManageServer;

            try
            {
                var request = new RestRequest("auth/oauth2/token");

                //add GetToken() API method parameters
                request.AddParameter("grant_type", "password");
                request.AddParameter("username", _user);
                request.AddParameter("password", new NetworkCredential("", _password).Password);
                request.AddParameter("client_id", _clientToken);
                request.AddParameter("client_secret", _clientSecret);
                request.AddParameter("scope", _persona);


                if (!_iManageServer.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase))
                    _iManageServer = "https://" + _iManageServer;

                (int status, string json) = imanageCon.restPost(_iManageServer, request);

                if (status == 200)
                {
                    accessToken = JsonConvert.DeserializeObject<authToken>(json);
                    connectionStatus = true;
                }

                getCriticalInfo();
            }
            catch (Exception ex) 
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }

        private void getCriticalInfo()
        {
            if (accessToken != null)
            {
                var request = new RestRequest("api");

                request.AddHeader("X-Auth-Token", accessToken.AccessToken);

                (int status, string json) = imanageCon.restGet(baseUrl, request);

                if (status == 200)
                {
                    imanSession = JsonConvert.DeserializeObject<imanSessionInfo>(json);
                    connectionStatus = true;
                }
            }
            else
            {
                imanSession = null;
            }
        }

        public (int,string) restV2QueryGet(RestRequest request )
        {
            if (imanSession == null)
                return (-1, "");

            request.Resource = String.Format("work/api/v2/customers/" + imanSession.Data.User.CustomerId + request.Resource.ToString(),imanSession.Data.Work.PreferredLibrary);
            request.AddHeader("X-Auth-Token", accessToken.AccessToken);

            return imanageCon.restGet(baseUrl,request);

        }

        public (int, string) restV2QueryPatch(RestRequest request)
        {
            if (imanSession == null)
                return (-1, "");

            request.Resource = String.Format("work/api/v2/customers/" + imanSession.Data.User.CustomerId + request.Resource.ToString(), imanSession.Data.Work.PreferredLibrary);
            request.AddHeader("X-Auth-Token", accessToken.AccessToken);

            return imanageCon.restPatch(baseUrl, request);
        }
    }
}
