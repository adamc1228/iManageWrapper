﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.classes.imanageCustom.rest
{
    internal class customRestClient
    {
        public (int,string) restPost(string serverURL, RestRequest request)
        {
            // Force TLS 1.2 instead of the default value.
            ServicePointManager.ServerCertificateValidationCallback = (s, cert, chain, ssl) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //create RestSharp client and POST request object
            var client = new RestClient(serverURL);

            //make the API request and get the response
            RestResponse response = client.Execute(request, Method.Post);

            return ((int)response.StatusCode,response.Content);
        }

        public (int, string) restGet(string serverURL, RestRequest request)
        {
            // Force TLS 1.2 instead of the default value.
            ServicePointManager.ServerCertificateValidationCallback = (s, cert, chain, ssl) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //create RestSharp client and POST request object
            var client = new RestClient(serverURL);

            //make the API request and get the response
            RestResponse response = client.Execute(request, Method.Get);

            return ((int)response.StatusCode, response.Content);
        }

        public (int, string) restPatch(string serverURL, RestRequest request)
        {
            // Force TLS 1.2 instead of the default value.
            ServicePointManager.ServerCertificateValidationCallback = (s, cert, chain, ssl) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //create RestSharp client and POST request object
            var client = new RestClient(serverURL);

            //make the API request and get the response
            RestResponse response = client.Execute(request, Method.Patch);

            return ((int)response.StatusCode, response.Content);
        }

        public void logoutRest(string serverUrl)
        {
            throw new NotImplementedException();
        }
    }
}
