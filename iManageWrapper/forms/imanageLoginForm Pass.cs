﻿using System;
using System.Net;
using System.Security;
using System.Windows.Forms;
using iManageWrapper.classes.core;

namespace iManageWrapper
{
    internal partial class imanageLoginFormPass : Form
    {
        public imanageSettings appSettings;
        internal SecureString passCode;

        public imanageLoginFormPass()
        {
            InitializeComponent();
            appSettings = new imanageSettings();
            loadSettings();
        }

        public imanageLoginFormPass(imanageSettings settings)
        {
            InitializeComponent();
            appSettings = settings;
            loadSettings();
        }

        private void btnSaveAndCLose_Click(object sender, EventArgs e)
        {
            passCode = new NetworkCredential("", mtbPassword.Text).SecurePassword;
            mtbPassword.Text = "f9023nv92fk23490fbvm2309";
            if (updateSettings())
                this.Close();
        }

        internal bool updateSettings()
        {
            //Imanage
            appSettings.accountInfo_Imanage_Secret = tbImanageSecret.Text;
            appSettings.accountInfo_Imanage_Server = tbImanageServer.Text;
            appSettings.accountInfo_Imanage_Client = tbImanageClient.Text;
          
            return appSettings.saveSettings();
        }

        internal void loadSettings()
        {
            //Imanage
            tbImanageSecret.Text = appSettings.accountInfo_Imanage_Secret;
            tbImanageServer.Text = appSettings.accountInfo_Imanage_Server;
            tbImanageClient.Text = appSettings.accountInfo_Imanage_Client;

            updateForm();
        }

        private void updateForm()
        {

        }
        private void cbImanageOAuth2_CheckedChanged(object sender, EventArgs e)
        {
            updateForm();
        }

        private void integrationsForm_Shown(object sender, EventArgs e)
        {
            loadSettings();
        }

        private void imanageLoginForm_Load(object sender, EventArgs e)
        {
            loadSettings();
        }

        private void gbIManage_Enter(object sender, EventArgs e)
        {

        }
    }
}
