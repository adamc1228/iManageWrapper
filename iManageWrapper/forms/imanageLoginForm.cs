﻿using System;
using System.Windows.Forms;
using iManageWrapper.classes.core;

namespace iManageWrapper
{
    internal partial class imanageLoginForm : Form
    {
        public imanageSettings appSettings;

        public imanageLoginForm()
        {
            InitializeComponent();
            appSettings = new imanageSettings();
            loadSettings();
        }

        public imanageLoginForm(imanageSettings settings)
        {
            InitializeComponent();
            appSettings = settings;
            loadSettings();
        }

        private void btnSaveAndCLose_Click(object sender, EventArgs e)
        {
            if(updateSettings())
                this.Close();
        }

        internal bool updateSettings()
        {
            //Imanage
            appSettings.accountInfo_Imanage_Server = tbImanageServer.Text;
            appSettings.accountInfo_Imanage_Client = tbImanageClient.Text;
          
            return appSettings.saveSettings();
        }

        internal void loadSettings()
        {
            //Imanage
            tbImanageServer.Text = appSettings.accountInfo_Imanage_Server;
            tbImanageClient.Text = appSettings.accountInfo_Imanage_Client;

            updateForm();
        }

        private void updateForm()
        {

        }
        private void cbImanageOAuth2_CheckedChanged(object sender, EventArgs e)
        {
            updateForm();
        }

        private void integrationsForm_Shown(object sender, EventArgs e)
        {
            loadSettings();
        }

        private void imanageLoginForm_Load(object sender, EventArgs e)
        {
            loadSettings();
        }

        private void gbIManage_Enter(object sender, EventArgs e)
        {

        }
    }
}
