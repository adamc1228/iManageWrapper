﻿namespace iManageWrapper
{
    partial class imanageLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(imanageLoginForm));
            this.gbIManage = new System.Windows.Forms.GroupBox();
            this.gbIManageServerInfo = new System.Windows.Forms.GroupBox();
            this.tbImanageClient = new System.Windows.Forms.TextBox();
            this.OAuth2 = new System.Windows.Forms.Label();
            this.tbImanageServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveAndCLose = new System.Windows.Forms.Button();
            this.filePickerDialog = new System.Windows.Forms.OpenFileDialog();
            this.gbIManage.SuspendLayout();
            this.gbIManageServerInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbIManage
            // 
            this.gbIManage.Controls.Add(this.gbIManageServerInfo);
            this.gbIManage.Location = new System.Drawing.Point(12, 12);
            this.gbIManage.Name = "gbIManage";
            this.gbIManage.Size = new System.Drawing.Size(405, 116);
            this.gbIManage.TabIndex = 0;
            this.gbIManage.TabStop = false;
            this.gbIManage.Text = "iManage";
            this.gbIManage.Enter += new System.EventHandler(this.gbIManage_Enter);
            // 
            // gbIManageServerInfo
            // 
            this.gbIManageServerInfo.Controls.Add(this.tbImanageClient);
            this.gbIManageServerInfo.Controls.Add(this.OAuth2);
            this.gbIManageServerInfo.Controls.Add(this.tbImanageServer);
            this.gbIManageServerInfo.Controls.Add(this.label1);
            this.gbIManageServerInfo.Location = new System.Drawing.Point(5, 19);
            this.gbIManageServerInfo.Name = "gbIManageServerInfo";
            this.gbIManageServerInfo.Size = new System.Drawing.Size(394, 82);
            this.gbIManageServerInfo.TabIndex = 2;
            this.gbIManageServerInfo.TabStop = false;
            this.gbIManageServerInfo.Text = "Server Info";
            // 
            // tbImanageClient
            // 
            this.tbImanageClient.Location = new System.Drawing.Point(70, 45);
            this.tbImanageClient.Name = "tbImanageClient";
            this.tbImanageClient.Size = new System.Drawing.Size(318, 20);
            this.tbImanageClient.TabIndex = 5;
            // 
            // OAuth2
            // 
            this.OAuth2.AutoSize = true;
            this.OAuth2.Location = new System.Drawing.Point(5, 48);
            this.OAuth2.Name = "OAuth2";
            this.OAuth2.Size = new System.Drawing.Size(50, 13);
            this.OAuth2.TabIndex = 4;
            this.OAuth2.Text = "Client ID:";
            // 
            // tbImanageServer
            // 
            this.tbImanageServer.Location = new System.Drawing.Point(70, 19);
            this.tbImanageServer.Name = "tbImanageServer";
            this.tbImanageServer.Size = new System.Drawing.Size(318, 20);
            this.tbImanageServer.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Server:";
            // 
            // btnSaveAndCLose
            // 
            this.btnSaveAndCLose.Location = new System.Drawing.Point(331, 143);
            this.btnSaveAndCLose.Name = "btnSaveAndCLose";
            this.btnSaveAndCLose.Size = new System.Drawing.Size(86, 30);
            this.btnSaveAndCLose.TabIndex = 0;
            this.btnSaveAndCLose.Text = "Save && Close";
            this.btnSaveAndCLose.UseVisualStyleBackColor = true;
            this.btnSaveAndCLose.Click += new System.EventHandler(this.btnSaveAndCLose_Click);
            // 
            // filePickerDialog
            // 
            this.filePickerDialog.FileName = "filePicker";
            // 
            // imanageLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 181);
            this.Controls.Add(this.btnSaveAndCLose);
            this.Controls.Add(this.gbIManage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(445, 220);
            this.MinimumSize = new System.Drawing.Size(445, 220);
            this.Name = "imanageLoginForm";
            this.Text = "Imanage Server Connection";
            this.Load += new System.EventHandler(this.imanageLoginForm_Load);
            this.Shown += new System.EventHandler(this.integrationsForm_Shown);
            this.gbIManage.ResumeLayout(false);
            this.gbIManageServerInfo.ResumeLayout(false);
            this.gbIManageServerInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbIManage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveAndCLose;
        private System.Windows.Forms.GroupBox gbIManageServerInfo;
        private System.Windows.Forms.TextBox tbImanageServer;
        private System.Windows.Forms.OpenFileDialog filePickerDialog;
        private System.Windows.Forms.TextBox tbImanageClient;
        private System.Windows.Forms.Label OAuth2;
    }
}