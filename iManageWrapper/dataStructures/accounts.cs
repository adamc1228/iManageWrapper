﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.dataStructures
{
    public class accounts
    {
        [JsonProperty("data")]
        public List<account> Items { get; set; }
    }

    public class account
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("allow_logon")]
        public bool AllowLogon { get; set; }

        [JsonProperty("user_num")]
        public int UserNum { get; set; }

        [JsonProperty("preferred_library")]
        public string PreferredLibrary { get; set; }

        [JsonProperty("user_nos")]
        public int UserNos { get; set; }

        [JsonProperty("is_external")]
        public bool IsExternal { get; set; }

        [JsonProperty("exch_autodiscover")]
        public string ExchAutodiscover { get; set; }

        [JsonProperty("wstype")]
        public string Wstype { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("userid")]
        public string Userid { get; set; }

        [JsonProperty("preferred_database")]
        public string PreferredDatabase { get; set; }

        [JsonProperty("login")]
        public bool Login { get; set; }

        [JsonProperty("database")]
        public string Database { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("fax")]
        public string Fax { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("user_domain")]
        public string UserDomain { get; set; }

        [JsonProperty("sync_id")]
        public string SyncId { get; set; }

        [JsonProperty("distinguished_name")]
        public string DistinguishedName { get; set; }
    }
}

