﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.classes.dataStructures
{
    [Serializable]
    public class workspace : clientMatter
    {
        [JsonProperty("author")]
        public string w_Author { get; set; }

        [JsonProperty("class")]
        public string w_Class { get; set; }

        [JsonProperty("class_description")]
        public string w_Class_Description { get; set; }

        [JsonProperty("content_type")]
        public string w_Content_Type { get; set; }

        [JsonProperty("create_date")]
        public string w_CreateDate { get; set; }

        [JsonProperty("custom1")]
        public string w_Custom1 { get; set; }

        [JsonProperty("custom1_description")]
        public string w_Custom1_Description { get; set; }

        [JsonProperty("custom2")]
        public string w_Custom2 { get; set; }

        [JsonProperty("custom2_description")]
        public string w_Custom2_Description { get; set; }

        [JsonProperty("database")]
        public string w_Database { get; set; }

        [JsonProperty("declared")]
        public string w_Declared { get; set; }

        [JsonProperty("default_security")]
        public string w_Default_Security { get; set; }

        [JsonProperty("description")]
        public string w_Description { get; set; }

        [JsonProperty("document_number")]
        public string w_Document_Number { get; set; }

        [JsonProperty("edit_date")]
        public string w_Edit_Date { get; set; }

        [JsonProperty("edit_profile_date")]
        public string w_Edit_Profile_Date { get; set; }

        [JsonProperty("effective_security")]
        public string w_Effective_Security { get; set; }

        [JsonProperty("extension")]
        public string w_Extension { get; set; }

        [JsonProperty("file_create_date")]
        public DateTime w_FileCreateDate { get; set; }

        [JsonProperty("file_edit_date")]
        public DateTime w_FileEditDate { get; set; }

        [JsonProperty("has_subfolders")]
        public bool w_HasSubfolders { get; set; }

        [JsonProperty("id")]
        public string w_Id { get; set; }

        [JsonProperty("in_use")]
        public bool w_InUse { get; set; }

        [JsonProperty("indexable")]
        public bool w_Indexable { get; set; }

        [JsonProperty("is_checked_out")]
        public bool w_IsCheckedOut { get; set; }

        [JsonProperty("is_container_saved_search")]
        public bool w_IsContainerSavedSearch { get; set; }

        [JsonProperty("is_content_saved_search")]
        public bool w_IsContentSavedSearch { get; set; }

        [JsonProperty("is_declared")]
        public bool w_IsDeclared { get; set; }

        [JsonProperty("is_external")]
        public bool w_IsExternal { get; set; }

        [JsonProperty("is_external_as_normal")]
        public bool w_IsExternalAsNormal { get; set; }

        [JsonProperty("is_hidden")]
        public bool w_IsHidden { get; set; }

        [JsonProperty("is_hipaa")]
        public bool w_IsHipaa { get; set; }

        [JsonProperty("is_in_use")]
        public bool w_IsInUse { get; set; }

        [JsonProperty("is_related")]
        public bool w_IsRelated { get; set; }

        [JsonProperty("is_restorable")]
        public bool w_IsRestorable { get; set; }

        [JsonProperty("iwl")]
        public string w_Iwl { get; set; }

        [JsonProperty("last_user")]
        public string w_LastUser { get; set; }

        [JsonProperty("name")]
        public string w_Name { get; set; }

        [JsonProperty("operator")]
        public string w_Operator { get; set; }

        [JsonProperty("owner")]
        public string w_Owner { get; set; }

        [JsonProperty("project_custom3")]
        public string w_ProjectCustom3 { get; set; }

        [JsonProperty("retain_days")]
        public int w_RetainDays { get; set; }

        [JsonProperty("size")]
        public int w_Size { get; set; }

        [JsonProperty("subtype")]
        public string w_Subtype { get; set; }

        [JsonProperty("type")]
        public string w_Type { get; set; }

        [JsonProperty("type_description")]
        public string w_TypeDescription { get; set; }

        [JsonProperty("version")]
        public int w_Version { get; set; }

        [JsonProperty("workspace_id")]
        public string w_WorkspaceId { get; set; }

        [JsonProperty("wstype")]
        public string w_Wstype { get; set; }

        public workspace()
        {
            this.w_Author = "";
            this.w_Class = "";
            this.w_Class_Description = "";
            this.w_Content_Type = "";
            this.w_CreateDate = "";
            this.w_Custom1 = "";
            this.w_Custom1_Description = "";
            this.w_Custom2 = "";
            this.w_Custom2_Description = "";
            this.w_Database = "";
            this.w_Declared = "";
            this.w_Default_Security = "";
            this.w_Description = "";
            this.w_Document_Number = "";
            this.w_Edit_Date = "";
            this.w_Edit_Profile_Date = "";
            this.w_Effective_Security = "";
            this.w_Extension = "";
            this.w_FileCreateDate = w_FileCreateDate;
            this.w_FileEditDate = w_FileEditDate;
            this.w_HasSubfolders = w_HasSubfolders;
            this.w_Id = "";
            this.w_InUse = w_InUse;
            this.w_Indexable = w_Indexable;
            this.w_IsCheckedOut = w_IsCheckedOut;
            this.w_IsContainerSavedSearch = w_IsContainerSavedSearch;
            this.w_IsContentSavedSearch = w_IsContentSavedSearch;
            this.w_IsDeclared = w_IsDeclared;
            this.w_IsExternal = w_IsExternal;
            this.w_IsExternalAsNormal = w_IsExternalAsNormal;
            this.w_IsHidden = w_IsHidden;
            this.w_IsHipaa = w_IsHipaa;
            this.w_IsInUse = w_IsInUse;
            this.w_IsRelated = w_IsRelated;
            this.w_IsRestorable = w_IsRestorable;
            this.w_Iwl = "";
            this.w_LastUser = "";
            this.w_Name = "";
            this.w_Operator = "";
            this.w_Owner = "";
            this.w_ProjectCustom3 = "";
            this.w_RetainDays = w_RetainDays;
            this.w_Size = w_Size;
            this.w_Subtype = "";
            this.w_Type = "";
            this.w_TypeDescription = "";
            this.w_Version = w_Version;
            this.w_WorkspaceId = "";
            this.w_Wstype = "";
        }

        public workspace(string w_Author, string w_Class, string w_Class_Description, string w_Content_Type, string w_CreateDate, string w_Custom1, string w_Custom1_Description, string w_Custom2, string w_Custom2_Description, string w_Database, string w_Declared, string w_Default_Security, string w_Description, string w_Document_Number, string w_Edit_Date, string w_Edit_Profile_Date, string w_Effective_Security, string w_Extension, DateTime w_FileCreateDate, DateTime w_FileEditDate, bool w_HasSubfolders, string w_Id, bool w_InUse, bool w_Indexable, bool w_IsCheckedOut, bool w_IsContainerSavedSearch, bool w_IsContentSavedSearch, bool w_IsDeclared, bool w_IsExternal, bool w_IsExternalAsNormal, bool w_IsHidden, bool w_IsHipaa, bool w_IsInUse, bool w_IsRelated, bool w_IsRestorable, string w_Iwl, string w_LastUser, string w_Name, string w_Operator, string w_Owner, string w_ProjectCustom3, int w_RetainDays, int w_Size, string w_Subtype, string w_Type, string w_TypeDescription, int w_Version, string w_WorkspaceId, string w_Wstype)
        {
            this.w_Author = w_Author ?? "";
            this.w_Class = w_Class ?? throw new ArgumentNullException(nameof(w_Class));
            this.w_Class_Description = w_Class_Description ?? "";
            this.w_Content_Type = w_Content_Type ?? "";
            this.w_CreateDate = w_CreateDate ?? "";
            this.w_Custom1 = w_Custom1 ?? "";
            this.w_Custom1_Description = w_Custom1_Description ?? "";
            this.w_Custom2 = w_Custom2 ?? "";
            this.w_Custom2_Description = w_Custom2_Description ?? "";
            this.w_Database = w_Database ?? "";
            this.w_Declared = w_Declared ?? "";
            this.w_Default_Security = w_Default_Security ?? "";
            this.w_Description = w_Description ?? "";
            this.w_Document_Number = w_Document_Number ?? "";
            this.w_Edit_Date = w_Edit_Date ?? "";
            this.w_Edit_Profile_Date = w_Edit_Profile_Date ?? "";
            this.w_Effective_Security = w_Effective_Security ?? "";
            this.w_Extension = w_Extension ?? "";
            this.w_FileCreateDate = w_FileCreateDate;
            this.w_FileEditDate = w_FileEditDate;
            this.w_HasSubfolders = w_HasSubfolders;
            this.w_Id = w_Id ?? "";
            this.w_InUse = w_InUse;
            this.w_Indexable = w_Indexable;
            this.w_IsCheckedOut = w_IsCheckedOut;
            this.w_IsContainerSavedSearch = w_IsContainerSavedSearch;
            this.w_IsContentSavedSearch = w_IsContentSavedSearch;
            this.w_IsDeclared = w_IsDeclared;
            this.w_IsExternal = w_IsExternal;
            this.w_IsExternalAsNormal = w_IsExternalAsNormal;
            this.w_IsHidden = w_IsHidden;
            this.w_IsHipaa = w_IsHipaa;
            this.w_IsInUse = w_IsInUse;
            this.w_IsRelated = w_IsRelated;
            this.w_IsRestorable = w_IsRestorable;
            this.w_Iwl = w_Iwl ?? "";
            this.w_LastUser = w_LastUser ?? "";
            this.w_Name = w_Name ?? "";
            this.w_Operator = w_Operator ?? "";
            this.w_Owner = w_Owner ?? "";
            this.w_ProjectCustom3 = w_ProjectCustom3 ?? "";
            this.w_RetainDays = w_RetainDays;
            this.w_Size = w_Size;
            this.w_Subtype = w_Subtype ?? "";
            this.w_Type = w_Type ?? "";
            this.w_TypeDescription = w_TypeDescription ?? "";
            this.w_Version = w_Version;
            this.w_WorkspaceId = w_WorkspaceId ?? "";
            this.w_Wstype = w_Wstype ?? "";
        }
    }
}
