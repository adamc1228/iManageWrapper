﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.dataStructures
{
    public class docHistory
    {
        [JsonProperty("data")]
        public List<docHistoryRecord> Records { get; set; }
    }

    public class docHistoryRecord
    {
        [JsonProperty("activity")]
        public string Activity { get; set; }

        [JsonProperty("activity_code")]
        public int ActivityCode { get; set; }

        [JsonProperty("activity_date")]
        public DateTime ActivityDate { get; set; }

        [JsonProperty("application_name")]
        public string ApplicationName { get; set; }

        [JsonProperty("document_number")]
        public int DocumentNumber { get; set; }

        [JsonProperty("has_journal")]
        public bool HasJournal { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("num1")]
        public string Num1 { get; set; }

        [JsonProperty("sid")]
        public int Sid { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("user_fullname")]
        public string UserFullname { get; set; }

        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("duration")]
        public int? Duration { get; set; }

    }
}
