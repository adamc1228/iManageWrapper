﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.classes.dataStructures
{

    //Attorney Overrides. 
    //Some Attorneys want a specific folder structure every time.
    [Serializable]
    public class attorneyOverride
    {

        public string attorneyCode { get; set; }
        public string matterType { get; set; }
        public string attorneyName { get; set; }


        public attorneyOverride(string _attorneyCode, string _matterCode, string _attorneyName)
        {
            attorneyCode = _attorneyCode;
            matterType = _matterCode;
            attorneyName = _attorneyName;
        }
    }

    //Client Overrides
    //Some Clients should always open with a specific matter type.
    [Serializable]
    public class clientOverride
    {

        public string clientCode { get; set; }
        public string matterType { get; set; }
        public string clientName { get; set; }


        public clientOverride(string _clientCode, string _matterType, string _clientName)
        {
            clientCode = _clientCode;
            matterType = _matterType;
            clientName = _clientName;
        }
    }

    //Imanage Template
    //Standard template mappings for users
    [Serializable]
    public class imanageTemplate
    {
        public string matterType { get; set; }
        public string imanageMatterID { get; set; }
        public string flexFolderMap { get; set; }
        public string officeLoction { get; set; }
        public string imanageIdentifier { get; set; }
        public string description { get; set; }

        public imanageTemplate(string _matterType, string _flexFolderMap, string _imanMatterID, string _imanIdent, string _description, string _officeLocation = "")
        {
            matterType = _matterType;
            flexFolderMap = _flexFolderMap;
            imanageMatterID = _imanMatterID;
            imanageIdentifier = _imanIdent;
            description = _description;
            officeLoction = _officeLocation;
        }
    }

    //Name Format Overrides
    //Some Matters open with a different naming convention.
    [Serializable]
    public class nameAliasOverride
    {
        public string matterType { get; set; }
        public string alias { get; set; }

        public nameAliasOverride(string _matterType, string _alias)
        {
            matterType = _matterType;
            alias = _alias;
        }
    }
}
