﻿using iManageWrapper.classes.dataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.classes.dataStructures
{
    [Serializable]
    public class clientMatter
    {

        public string cm_Client { get; set; }
        public string cm_Matter { get; set; }
        public string cm_ParentAlias { get; set; }
        public string cm_CustomAlias { get; set; }
        public string cm_OpenDate { get; set; }
        public string cm_Attorney { get; set; }
        public string cm_AttorneyOffice { get; set; }
        public int cm_AttorneyID { get; set; }
        public string cm_OrigType { get; set; }
        public string cm_NewType { get; set; }
        public bool cm_OpenInImanage { get; set; }
        public bool cm_Processed { get; set; }

        public string cm_NameAliasTemplate { get; set; }
        public imanageTemplate cm_ImanageTemplate { get; set; }

        public clientMatter()
        {
            this.cm_Client = "";
            this.cm_Matter = "";
            this.cm_ParentAlias = "";
            this.cm_CustomAlias = "";
            this.cm_OpenDate = "";
            this.cm_Attorney = "";
            this.cm_OrigType = "";
            this.cm_NewType = "";
            this.cm_OpenInImanage = false;
            this.cm_Processed = false;
            this.cm_AttorneyID = -1;
            this.cm_AttorneyOffice = "";
            this.cm_ImanageTemplate = null;
            this.cm_NameAliasTemplate = "";
        }

        //Default constructor with params
        public clientMatter(string _client, string _matter)
        {
            this.cm_Client = String.IsNullOrEmpty(_client) ? "" : _client;
            this.cm_Matter = String.IsNullOrEmpty(_matter) ? "" : _matter;
            this.cm_ParentAlias = "";
            this.cm_CustomAlias = "";
            this.cm_OpenDate = "";
            this.cm_Attorney = "";
            this.cm_OrigType = "";
            this.cm_NewType = "";
            this.cm_OpenInImanage = false;
            this.cm_Processed = false;
            this.cm_AttorneyID = -1;
            this.cm_AttorneyOffice = "";
            this.cm_ImanageTemplate = null;
            this.cm_NameAliasTemplate = "";
        }

        public clientMatter(string _client, string _matter,string _parentAlias, string _customAlias)
        {
            this.cm_Client = String.IsNullOrEmpty(_client) ? "" : _client;
            this.cm_Matter = String.IsNullOrEmpty(_matter) ? "" : _matter;
            this.cm_ParentAlias = String.IsNullOrEmpty(_parentAlias) ? "" : _parentAlias;
            this.cm_CustomAlias = String.IsNullOrEmpty(_customAlias) ? "" : _customAlias;
            this.cm_OpenDate = "";
            this.cm_Attorney = "";
            this.cm_OrigType = "";
            this.cm_NewType = "";
            this.cm_OpenInImanage = false;
            this.cm_Processed = false;
            this.cm_AttorneyID = -1;
            this.cm_AttorneyOffice = "";
            this.cm_ImanageTemplate = null;
            this.cm_NameAliasTemplate = "";
        }

        //Default constructor with params
        public clientMatter(string _client, string _matter, string _openDate)
        {
            this.cm_Client = String.IsNullOrEmpty(_client) ? "" : _client;
            this.cm_Matter = String.IsNullOrEmpty(_matter) ? "" : _matter;
            this.cm_ParentAlias = "";
            this.cm_CustomAlias = "";
            this.cm_OpenDate = String.IsNullOrEmpty(_openDate) ? "" : _openDate;
            this.cm_Attorney = "";
            this.cm_OrigType = "";
            this.cm_NewType = "";
            this.cm_OpenInImanage = false;
            this.cm_Processed = false;
            this.cm_AttorneyID = -1;
            this.cm_AttorneyOffice = "";
            this.cm_ImanageTemplate = null;
            this.cm_NameAliasTemplate = "";
        }

        //Default constructor with params
        public clientMatter(string _client, string _matter, string _openDate, string _origType, string _attorney, int _attorneyID, string _attorneyOffice)
        {
            this.cm_Client = String.IsNullOrEmpty(_client) ? "" : _client;
            this.cm_Matter = String.IsNullOrEmpty(_matter) ? "" : _matter;
            this.cm_ParentAlias = "";
            this.cm_CustomAlias = "";
            this.cm_OpenDate = String.IsNullOrEmpty(_openDate) ? "" : _openDate;
            this.cm_Attorney = String.IsNullOrEmpty(_attorney) ? "" : _attorney;
            this.cm_OrigType = String.IsNullOrEmpty(_origType) ? "" : _origType;
            this.cm_NewType = "";
            this.cm_OpenInImanage = false;
            this.cm_Processed = false;
            this.cm_AttorneyID = _attorneyID;
            this.cm_AttorneyOffice = String.IsNullOrEmpty(_attorneyOffice) ? "" : _attorneyOffice;
            this.cm_ImanageTemplate = null;
            this.cm_NameAliasTemplate = "";
        }

        public clientMatter(string _client, string _matter, string _openDate, string _origType, string _attorney, int _attorneyID, string _attorneyOffice, string _cm_ParentAlias, string _cm_CustomAlias)
        {
            this.cm_Client = String.IsNullOrEmpty(_client) ? "" : _client;
            this.cm_Matter = String.IsNullOrEmpty(_matter) ? "" : _matter;
            this.cm_ParentAlias = _cm_ParentAlias;
            this.cm_CustomAlias = _cm_CustomAlias;
            this.cm_OpenDate = String.IsNullOrEmpty(_openDate) ? "" : _openDate;
            this.cm_Attorney = String.IsNullOrEmpty(_attorney) ? "" : _attorney;
            this.cm_OrigType = String.IsNullOrEmpty(_origType) ? "" : _origType;
            this.cm_NewType = "";
            this.cm_OpenInImanage = false;
            this.cm_Processed = false;
            this.cm_AttorneyID = _attorneyID;
            this.cm_AttorneyOffice = String.IsNullOrEmpty(_attorneyOffice) ? "" : _attorneyOffice;
            this.cm_ImanageTemplate = null;
            this.cm_NameAliasTemplate = "";
        }


        public string getClientMatterCode()
        {
            return this.cm_Client + "." + this.cm_Matter;
        }

        public string getCurrentMatterType()
        {
            if (String.IsNullOrEmpty(this.cm_NewType))
                return this.cm_OrigType;
            else
                return this.cm_NewType;
        }

        public string getFormattedName()
        {
            if (String.IsNullOrEmpty(cm_NameAliasTemplate))
                return String.Empty;

            return cm_NameAliasTemplate
                    .Replace("%C1ALIAS%", this.cm_Client)
                    .Replace("%C2ALIAS%", this.cm_Matter)
                    .Replace("%C1DESCR%", this.cm_ParentAlias)
                    .Replace("%C2DESCR%", this.cm_CustomAlias);
        }
    }
}
