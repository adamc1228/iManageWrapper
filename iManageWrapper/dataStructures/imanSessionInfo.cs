﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iManageWrapper.dataStructures
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

    internal class imanSessionInfo
    {
        [JsonProperty("data")]
        public Data Data { get; set; }

        public authToken authToken { get; set; }
    }

    internal class Data
    {
        [JsonProperty("auth_status")]
        public string AuthStatus { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("dms_version")]
        public string DmsVersion { get; set; }

        [JsonProperty("work")]
        public Work Work { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("capabilities")]
        public List<string> Capabilities { get; set; }

        [JsonProperty("versions")]
        public List<Version> Versions { get; set; }
    }

    internal class Library
    {
        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("is_hidden")]
        public bool IsHidden { get; set; }
    }

    internal class User
    {
        [JsonProperty("customer_id")]
        public int CustomerId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }

    internal class Version
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("version")]
        public string ApiVersion { get; set; }
    }

    internal class Work
    {
        [JsonProperty("preferred_library")]
        public string PreferredLibrary { get; set; }

        [JsonProperty("libraries")]
        public List<Library> Libraries { get; set; }
    }
}
